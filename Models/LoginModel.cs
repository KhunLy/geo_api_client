﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiConsole.Models
{
    class LoginModel
    {
        public string Pseudo { get; set; }
        public string Password { get; set; }
    }
}
