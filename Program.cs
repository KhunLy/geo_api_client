﻿using ApiConsole.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ApiConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string token = null;
            while(token == null)
            {
                Console.WriteLine("Entrez votre nom");
                string name = Console.ReadLine();

                Console.WriteLine("Entrez votre password");
                string pwd = Console.ReadLine();

                token = Login(new LoginModel { Pseudo = name, Password = pwd });
            }

            Console.Clear();

            IEnumerable<ContactModel> contacts = GetContact(token);

            foreach (var item in contacts)
            {
                Console.WriteLine(item.FirstName);
                Console.WriteLine(item.LastName);
            }

            Console.ReadLine();
        }

        static string Login(LoginModel model)
        {
            using (HttpClient client = new HttpClient())
            {
                StringContent content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                HttpResponseMessage rep = client.PostAsync("http://localhost:37322/api/user/login", content).Result;
                if(rep.IsSuccessStatusCode)
                {
                    string json = rep.Content.ReadAsStringAsync().Result;
                    return json;
                }
                return null;
            }
        }

        static IEnumerable<ContactModel> GetContact(string token)
        {
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                HttpResponseMessage rep = client.GetAsync("http://localhost:37322/api/contact").Result;
                if (rep.IsSuccessStatusCode)
                {
                    string json = rep.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<IEnumerable<ContactModel>>(json);
                }
                return null;
            }
        }
    }
}
